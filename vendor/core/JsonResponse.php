<?php

namespace vendor\core;

/**
 * Description of JsonResponse
 */
class JsonResponse
{
    protected $message;
    protected $status;
    protected $statusCode;
    protected $header = 'Content-Type: application/json';

    /**
     * JsonResponse constructor.
     * @param string $status
     * @param string $message
     * @param int $statusCode
     */
    public function __construct(string $status, string $message, int $statusCode)
    {
        $this->setStatus($status);
        $this->setMessage($message);
        $this->setStatusCode($statusCode);
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        header($this->header);
        http_response_code($this->statusCode);
        $response = [
            'status' => $this->status,
            'message' => $this->message
        ];
        return json_encode($response);
    }
}