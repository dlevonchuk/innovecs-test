<?php

namespace vendor\core;

/**
 * Description of Router
 */
class Router
{

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $routesPath = APP . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param string $uri входящий URL
     * @return bool
     */
    public function dispatch($uri)
    {
        foreach ($this->routes as $uriPattern => $path) {

            if (preg_match("~$uriPattern~", $uri)) {

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;

                $controllerClass = 'app\controllers\\' . $controllerName;
                if (class_exists($controllerClass)) {
                    $controllerObject = new $controllerClass;
                    call_user_func_array(array($controllerObject, $actionName), $parameters);
                    return true;
                }

            }
        }
        echo new JsonResponse('Not found', 'Route not found', 404);
        return false;
    }
}
