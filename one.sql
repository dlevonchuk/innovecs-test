SELECT email, SUM(amount) as amount_sum
FROM transactions
GROUP BY email
WHERE status = 'approved'
AND MONTH(create_date) = MONTH(CURRENT_DATE())
AND YEAR(create_date) = YEAR(CURRENT_DATE())