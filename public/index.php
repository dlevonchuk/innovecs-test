<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

use vendor\core\Router;
use vendor\core\Auth;
use vendor\core\JsonResponse;

$query = trim($_SERVER['REQUEST_URI'], '/');
define('ROOT', dirname(__DIR__));
define('APP', dirname(__DIR__) . '/app');

require '../vendor/libs/functions.php';
spl_autoload_register(function($class){
    $file = ROOT . '/' . str_replace('\\', '/', $class) . '.php';
    if(is_file($file)){
        require_once $file;
    }
});
if(!Auth::isAuth()){
    echo new JsonResponse('Forbidden', 'Access is denied', 403);
    die;
}
$router = new Router();
$router->dispatch($query);