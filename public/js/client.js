(function ($) {
	$(document).ready(function () {
		const TOKEN = 12345;
		let $responseBlock = $('.response'),
			$responseStatus = $('.status', $responseBlock),
			$responseMessage = $('.message', $responseBlock),
			$form = $('#form'),
			$email = $('input[name=email]', $form),
			$amount = $('input[name=amount]', $form);

		$form.submit(function (e) {
			let url = '/transaction/' + $email.val() + '/' + $amount.val();

			$.ajax({
				url: url,
				success: function (data, textStatus, jqXHR) {
					$responseBlock.css('background-color', 'green');
					shopResponse(data.status, data.message);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$responseBlock.css('background-color', 'red');
					shopResponse(jqXHR.responseJSON.status, jqXHR.responseJSON.message);
				},
				beforeSend: function (xhr, settings) {
					xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
				}
			});
			return false;
		});

		function shopResponse(status, message) {
			$responseStatus.html(status);
			$responseMessage.html(message);
		}
	});
})($);


