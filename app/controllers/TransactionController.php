<?php

namespace app\controllers;

use vendor\core\JsonResponse;

/**
 * Description of TransactionController
 */
class TransactionController
{
    const ACCEPT_TEXT = 'Success!';
    const REJECTED_TEXT = 'Fraud detected!';

    /**
     * @param string $email
     * @param float $amount
     */
    public function actionIndex(string $email, float $amount)
    {
        $randStatus = rand(0, 1);
        if ($randStatus) {
            $response = new JsonResponse('accepted', self::ACCEPT_TEXT, 200);
        } else {
            $response = new JsonResponse('rejected', self::REJECTED_TEXT, 400);
        }
        echo $response;
    }
}
